# Linear-regression-to-predict-number-of-upvotes-for-Reddits

This project covers the following points:

- Collect and save reddit data using the reddit API (through the praw library)
- Conduct descriptive analyses, via manipulation of data stored in a pandas dataframe, 
  and via the creation and exploration of graphs, of the number of upvotes of reddit comments
- Conduct a linear regression to help understand the factors associated with a top post having 
  many upvotes on reddit
- Implement additional feature sets and/or a new model and describe why those decisions were 
  made and what their effects were on performance
